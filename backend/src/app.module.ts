import { Module } from '@nestjs/common';
import {join} from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GraphQLModule } from '@nestjs/graphql';
import { RootResolver } from './resolvers/app.resolver.root';
import { UserResolver } from './resolvers/app.resolver.user';

import { Node } from './models/app.model.node';
import { PageInfo } from './models/app.model.page.info';
import { Root } from './models/app.model.root';
import { Todo } from './models/app.model.todo';
import { TodoConnection } from './models/app.model.todo.connection';
import { TodoEdge } from './models/app.model.todo.edge';
import { User } from './models/app.model.user';
import { AddTodoInput } from './models/app.model.add.todo.input';
import { AddTodoPayload } from './models/app.model.add.todo.payload';




@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
    }),
  ],
  controllers: [AppController],
  providers: [
    AppService,
    RootResolver,
    UserResolver,
    Node,
    PageInfo,
    Root,
    Todo,
    TodoConnection,
    TodoEdge,
    User,
    AddTodoInput,
    AddTodoPayload,
  ],
})
export class AppModule { }

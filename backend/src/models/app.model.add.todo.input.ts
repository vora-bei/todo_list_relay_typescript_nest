import { Field, InputType, ID } from '@nestjs/graphql';

@InputType()
export class AddTodoInput {

    @Field()
    text: string;

    @Field({nullable: true})
    clientMutationId: string;
    
}
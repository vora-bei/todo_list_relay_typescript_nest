import { Field, Int, ObjectType } from '@nestjs/graphql';
import { User } from "./app.model.user"
import { Node } from "./app.model.node"

@ObjectType()
export class Root {
    @Field(type => [Node])
    node: number;
    @Field(type => User)
    viewer: User
}
import { Field, InputType, ID } from '@nestjs/graphql';

@InputType()
export class RemoveTodoInput {
    @Field(type => ID)
    id: number;
   
    @Field({nullable: true})
    clientMutationId: string;
}
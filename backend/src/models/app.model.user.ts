import { Field, ID, ObjectType, Int } from '@nestjs/graphql';
import { TodoConnection } from "./app.model.todo.connection"
import { Node } from "./app.model.node"
@ObjectType()
export class User extends Node{
    @Field(type => ID)
    id: number;
    @Field(type => TodoConnection)
    todos: TodoConnection;

    @Field(type => Int)
    numTodos: number;
    
    @Field(type => Int)
    numCompletedTodos: number;
}
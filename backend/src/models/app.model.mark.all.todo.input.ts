import { Field, InputType, ID } from '@nestjs/graphql';

@InputType()
export class MarkAllTodosInput {

    @Field()
    complete: boolean;

    @Field({nullable: true})
    clientMutationId: string;
}
import { Field, Int, ID, ObjectType, InterfaceType } from '@nestjs/graphql';

@InterfaceType()
export class Node {
    @Field(type => ID)
    id: number;
}
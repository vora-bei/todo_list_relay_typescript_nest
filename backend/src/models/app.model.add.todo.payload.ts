import { Field, ObjectType, ID } from '@nestjs/graphql';
import { User } from './app.model.user';
import { TodoEdge } from './app.model.todo.edge';

@ObjectType()
export class AddTodoPayload {
    @Field(type => User)
    viewer: User;

    @Field(type => TodoEdge)
    todoEdge: TodoEdge;
    
    @Field({nullable: true})
    clientMutationId: String

}
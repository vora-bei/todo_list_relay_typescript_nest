import { Field, InputType, ID } from '@nestjs/graphql';

@InputType()
export class RenameTodoInput {

    @Field(type => ID)
    id: number;
    
    @Field()
    text: string;

    @Field({nullable: true})
    clientMutationId: string;
}
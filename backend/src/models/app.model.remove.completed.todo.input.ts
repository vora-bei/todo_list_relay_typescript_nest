import { Field, InputType, ID } from '@nestjs/graphql';

@InputType()
export class RemoveCompletedTodosInput {
    
    @Field({nullable: true})
    clientMutationId: string;
}
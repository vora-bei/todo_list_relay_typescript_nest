import { Field, ObjectType, ID } from '@nestjs/graphql';
import { User } from './app.model.user';

@ObjectType()
export class RemoveCompletedTodosPayload {
    @Field(type => User)
    viewer: User;

    @Field(type=>[ID])
    deletedIds: string[];
    
    @Field({nullable: true})
    clientMutationId: string

}
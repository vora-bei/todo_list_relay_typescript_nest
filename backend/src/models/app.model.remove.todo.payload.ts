import { Field, ObjectType, ID } from '@nestjs/graphql';
import { User } from './app.model.user';

@ObjectType()
export class RemoveTodoPayload {
    
    @Field(type => User)
    viewer: User;

    @Field(type => ID)
    deletedId: string;
    
    @Field({nullable: true})
    clientMutationId: String

}
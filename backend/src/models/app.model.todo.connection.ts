import { Field, Int, ObjectType } from '@nestjs/graphql';
import {PageInfo} from "./app.model.page.info";
import {TodoEdge} from "./app.model.todo.edge";
@ObjectType()
export class TodoConnection {
    @Field(type => PageInfo)
    pageInfo: PageInfo;

    @Field(type => [TodoEdge])
    edges: [TodoEdge]
}
import { Field, Int, ObjectType } from '@nestjs/graphql';
import {Todo} from './app.model.todo'
@ObjectType()
export class TodoEdge {
    @Field(type => Todo)
    node: Todo
    @Field()
    cursor!: string;
}
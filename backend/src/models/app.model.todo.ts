import { Field, ObjectType, ID } from '@nestjs/graphql';
import { Node } from './app.model.node';

@ObjectType()
export class Todo extends Node {
    @Field(type => ID)
    id: number;
    @Field()
    complete: boolean;

    @Field()
    text: string;
}
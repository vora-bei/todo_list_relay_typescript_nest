import { Field, InputType, ID } from '@nestjs/graphql';

@InputType()
export class ChangeTodoStatusInput {

    @Field(type => ID)
    id: number;

    @Field()
    complete: boolean;

    @Field({nullable: true})
    clientMutationId: string;
}
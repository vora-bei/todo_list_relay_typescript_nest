import { Field, ObjectType } from '@nestjs/graphql';
import { User } from './app.model.user';
import { Todo } from './app.model.todo';

@ObjectType()
export class ChangeTodoStatusPayload {
    @Field(type => User)
    viewer: User;

    @Field(type => Todo)
    todo: Todo;
    
    @Field({nullable: true})
    clientMutationId: String

}
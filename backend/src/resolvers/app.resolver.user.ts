import { Resolver, ResolveField, Args, Parent, Int, Mutation } from '@nestjs/graphql';
import { User } from '../models/app.model.user'

import { AddTodoInput } from '../models/app.model.add.todo.input';
import { AddTodoPayload } from '../models/app.model.add.todo.payload';

import { ChangeTodoStatusInput } from '../models/app.model.change.todo.status.input';
import { ChangeTodoStatusPayload } from '../models/app.model.change.todo.status.payload';

import { RemoveCompletedTodosInput } from '../models/app.model.remove.completed.todo.input';
import { RemoveCompletedTodosPayload } from '../models/app.model.remove.completed.todos.payload';

import { RemoveTodoInput } from '../models/app.model.remove.todo.input';
import { RemoveTodoPayload } from '../models/app.model.remove.todo.payload';

import { RenameTodoInput } from '../models/app.model.rename.todo.input';
import { RenameTodoPayload } from '../models/app.model.rename.todo.payload';

import { MarkAllTodosInput } from '../models/app.model.mark.all.todo.input';
import { MarkAllTodosPayload } from '../models/app.model.mark.all.todos.payload';


import {
  getTodos,
  addTodo,
  changeTodoStatus,
  removeCompletedTodos,
  removeTodo,
  renameTodo,
  markAllTodos,
  getTodo
} from './database'
import {
  connectionFromArray,
} from 'graphql-relay';

@Resolver(of => User)
export class UserResolver {

  @ResolveField()
  async todos(
    @Parent() author: User,
    @Args('status', { defaultValue: "any" }) status: string,
    @Args('after', { nullable: true }) after: String,
    @Args('first', { type: () => Int, nullable: true }) first: number,
    @Args('before', { nullable: true }) before: String,
    @Args('last', { type: () => Int, nullable: true }) last: number
  ) {
    return connectionFromArray(getTodos(status), [after, first, before, last]);
  }

  @ResolveField()
  async numTodos(
    @Parent() author: User,
  ) {
    return getTodos('any').length;
  }
  @ResolveField()
  async numCompletedTodos(
    @Parent() author: User,
  ) {
    return getTodos('completed').length;
  }

  @Mutation(returns => AddTodoPayload)
  addTodo(@Args({ name: 'input', type: () => AddTodoInput }) input: AddTodoInput) {
    const id = addTodo(input.text, false);
    return {
        todoEdge: {
          cursor: id,
          node: getTodo(id)
        },
        viewer: { 
          id: 'me'
        }
      }
  }

  @Mutation(returns => ChangeTodoStatusPayload)
  changeTodoStatus(@Args({ name: 'input', type: () => ChangeTodoStatusInput }) input: ChangeTodoStatusInput) {
    changeTodoStatus(input.id, input.complete);
    return {
      todo: getTodo(input.id),
      viewer: { 
        id: 'me'
      },
      clientMutationId: input.clientMutationId
    }
  }


  @Mutation(returns => MarkAllTodosPayload)
  markAllTodos(@Args({ name: 'input', type: () => MarkAllTodosInput }) input: MarkAllTodosInput) {
    const ids = getTodos('completed').map(({id})=>id);
    markAllTodos(input.complete);
    return {
      changedTodos: ids.map(id=> getTodo(id)),
      viewer: { 
        id: 'me'
      },
      clientMutationId: input.clientMutationId
    }
  }

  @Mutation(returns => RemoveCompletedTodosPayload)
  removeCompletedTodos(@Args({ name: 'input', type: () => RemoveCompletedTodosInput }) input: RemoveCompletedTodosInput) {
    const ids = getTodos('completed').map(({id})=>id);
    removeCompletedTodos();
    return {
      deletedIds: ids,
      viewer: { 
        id: 'me'
      },
      clientMutationId: input.clientMutationId
    }
  }

  @Mutation(returns => RemoveTodoPayload)
  removeTodo(@Args({ name: 'input', type: () => RemoveTodoInput }) input: RemoveTodoInput) {
    removeTodo(input.id);
    return {
      deletedId: input.id,
      viewer: { 
        id: 'me'
      },
      clientMutationId: input.clientMutationId
    }
    
  }
  @Mutation(returns => RenameTodoPayload)
  renameTodo(@Args({ name: 'input', type: () => RenameTodoInput }) input: RenameTodoInput) {
    renameTodo(input.id, input.text);
    return {
      todo: getTodo(input.id),
      viewer: { 
        id: 'me'
      },
      clientMutationId: input.clientMutationId
    }
  }

}
import {Resolver, Args, Query } from '@nestjs/graphql';
import {Root} from '../models/app.model.root';
import {User} from '../models/app.model.user';
import {Node} from '../models/app.model.node';
@Resolver(of => Root)
export class RootResolver {

  @Query(returns => User)
  async viewer() {
    return { id: "me" };
  }

  @Query(returns => Node)
  async node(@Args('id') id: string) {
    return { id };
  }

}
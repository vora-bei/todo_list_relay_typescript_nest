const { override, addBabelPlugins, addBabelPreset } = require('customize-cra')

module.exports = override(
    addBabelPreset(["@4c", {
        "target": "web-app",
        "useBuiltIns": "usage",
        "envCorejs": 3
    }]),
    addBabelPlugins(
        "relay"
    )
)
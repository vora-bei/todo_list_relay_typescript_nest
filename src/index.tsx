import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';

import HashProtocol from 'farce/HashProtocol';
import queryMiddleware from 'farce/queryMiddleware';
import { Resolver } from 'found-relay';
import createFarceRouter from 'found/createFarceRouter';
import { Environment, RecordSource, Store, Network } from 'relay-runtime';

import routes from './routes';

import 'todomvc-common';
import 'todomvc-common/base.css';
import 'todomvc-app-css/index.css';


function fetchQuery(
  operation,
  variables,
  cacheConfig,
  uploadables,
) {
  return fetch('/graphql', {
    method: 'POST',
    headers: {
      // Add authentication and other headers here
      'content-type': 'application/json'
    },
    body: JSON.stringify({
      query: operation.text, // GraphQL text from input
      variables,
    }),
  }).then(response => {
    return response.json();
  });
}
const network = Network.create(fetchQuery);

const environment = new Environment({
  network: network,
  store: new Store(new RecordSource()),
});

const Router = createFarceRouter({
  historyProtocol: new HashProtocol(),
  historyMiddlewares: [queryMiddleware],
  routeConfig: routes,
});

ReactDOM.render(
  <React.StrictMode>
    <Router resolver={new Resolver(environment)} />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

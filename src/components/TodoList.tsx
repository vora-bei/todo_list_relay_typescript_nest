import React from 'react';
import {
  ReactRelayContext,
  createFragmentContainer,
  graphql,
  RelayProp,
} from 'react-relay';

import MarkAllTodosMutation from '../mutations/MarkAllTodosMutation';
import Todo from './Todo';
import { TodoList_viewer } from './__generated__/TodoList_viewer.graphql';

interface IProps{
  relay: RelayProp;
  viewer: TodoList_viewer;
}

const contextType = ReactRelayContext;


class TodoList extends React.Component<IProps> {
  static contextType = contextType;
  onToggleAllChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { relay, viewer } = this.props;
    const complete = e.target.checked;

    MarkAllTodosMutation.commit(
      relay.environment,
      viewer,
      complete,
      this.context.variables.status,
    );
  };

  render() {
    const { viewer } = this.props;
    const { todos, numTodos, numCompletedTodos } = viewer;

    if (!numTodos) {
      return null;
    }

    return (
      <section className="main">
        <input
          id="toggle-all"
          type="checkbox"
          checked={numTodos === numCompletedTodos}
          className="toggle-all"
          onChange={this.onToggleAllChange}
        />
        <label htmlFor="toggle-all">Mark all as complete</label>

        <ul className="todo-list">
          {todos!.edges!.map((edge) => (
            <Todo key={edge!.node!.id} viewer={viewer} todo={edge!.node!} />
          ))}
        </ul>
      </section>
    );
  }
}

TodoList.contextType = contextType;

export default createFragmentContainer(TodoList, {
  viewer: graphql`
    fragment TodoList_viewer on User {
      todos(status: $status, first: 2147483647)
        @connection(key: "TodoList_todos") {
        edges {
          node {
            id
            complete
            ...Todo_todo
          }
        }
      }
      id
      numTodos
      numCompletedTodos
      ...Todo_viewer
    }
  `,
});

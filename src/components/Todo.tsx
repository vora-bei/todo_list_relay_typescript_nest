import classNames from 'classnames';
import React from 'react';
import { createFragmentContainer, graphql, RelayProp } from 'react-relay';

import ChangeTodoStatusMutation from '../mutations/ChangeTodoStatusMutation';
import RemoveTodoMutation from '../mutations/RemoveTodoMutation';
import RenameTodoMutation from '../mutations/RenameTodoMutation';
import TodoTextInput from './TodoTextInput';
import  { Todo_viewer } from './__generated__/Todo_viewer.graphql';
import  { Todo_todo } from './__generated__/Todo_todo.graphql'


interface IProps{
 viewer: Todo_viewer; 
 todo: Todo_todo;
 relay: RelayProp;
}
interface IState{
  isEditing: boolean;
}
 class Todo extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      isEditing: false,
    };
  }

  onCompleteChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { relay, viewer, todo } = this.props;
    const complete = e.target.checked;

    ChangeTodoStatusMutation.commit(relay.environment, viewer, todo, complete);
  };

  onDestroyClick = () => {
    this.removeTodo();
  };

  onLabelDoubleClick = () => {
    this.setEditMode(true);
  };

  onTextInputCancel = () => {
    this.setEditMode(false);
  };

  onTextInputDelete = () => {
    this.setEditMode(false);
    this.removeTodo();
  };

  onTextInputSave = (text: string) => {
    const { relay, todo } = this.props;

    this.setEditMode(false);

    RenameTodoMutation.commit(relay.environment, todo, text);
  };

  setEditMode(isEditing: boolean) {
    this.setState({ isEditing });
  }

  removeTodo() {
    const { relay, viewer, todo } = this.props;

    RemoveTodoMutation.commit(relay.environment, viewer, todo);
  }

  render() {
    const { complete, text } = this.props.todo;
    const { isEditing } = this.state;

    /* eslint-disable jsx-a11y/control-has-associated-label, jsx-a11y/label-has-associated-control */
    return (
      <li
        className={classNames({
          completed: complete,
          editing: isEditing,
        })}
      >
        <div className="view">
          <input
            type="checkbox"
            checked={complete || undefined}
            className="toggle"
            onChange={this.onCompleteChange}
          />
          <label onDoubleClick={this.onLabelDoubleClick}>{text}</label>
          <button
            type="button"
            className="destroy"
            onClick={this.onDestroyClick}
          />
        </div>

        {!!this.state.isEditing && (
          <TodoTextInput
            className="edit"
            commitOnBlur
            initialValue={this.props.todo.text}
            onCancel={this.onTextInputCancel}
            onDelete={this.onTextInputDelete}
            onSave={this.onTextInputSave}
          />
        )}
      </li>
    );
  }
}

export default createFragmentContainer(Todo, {
  viewer: graphql`
    fragment Todo_viewer on User {
      id
    }
  `,
  todo: graphql`
    fragment Todo_todo on Todo {
      id
      complete
      text
    }
  `,
});

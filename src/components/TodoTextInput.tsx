import keycode from 'keycode';
import React from 'react';

interface IProps{
  placeholder?: string;
  className: string;
  initialValue?: string | null;
  commitOnBlur: boolean;
  onCancel?: () => any;
  onDelete?: () => any;
  onSave: (newText: string) => any;
}
interface IState {
  text: string;
}

class TodoTextInput extends React.Component<IProps, IState> {
  static defaultProps = {
    commitOnBlur: true,
  }
  constructor(props: IProps) {
    super(props);

    this.state = {
      text: this.props.initialValue || '',
    };
  }

  onKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (this.props.onCancel && e.keyCode === keycode.codes.esc) {
      this.props.onCancel();
    } else if (e.keyCode === keycode.codes.enter) {
      this.commitChanges();
    }
  };

  onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ text: e.target.value });
  };

  onBlur = () => {
    if (this.props.commitOnBlur) {
      this.commitChanges();
    }
  };

  commitChanges() {
    const newText = this.state.text.trim();
    if (this.props.onDelete && !newText) {
      this.props.onDelete();
    } else if (this.props.onCancel && newText === this.props.initialValue) {
      this.props.onCancel();
    } else if (newText) {
      this.props.onSave(newText);
      this.setState({ text: '' });
    }
  }

  render() {
    const { placeholder, className } = this.props;

    return (
      <input
        onKeyDown={this.onKeyDown}
        onChange={this.onChange}
        onBlur={this.onBlur}
        value={this.state.text}
        placeholder={placeholder}
        className={className}
      />
    );
  }
}

export default TodoTextInput;
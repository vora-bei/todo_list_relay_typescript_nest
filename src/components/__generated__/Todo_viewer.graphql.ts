/* tslint:disable */
/* eslint-disable */

import { ReaderFragment } from "relay-runtime";
import { FragmentRefs } from "relay-runtime";
export type Todo_viewer = {
    readonly id: string;
    readonly " $refType": "Todo_viewer";
};
export type Todo_viewer$data = Todo_viewer;
export type Todo_viewer$key = {
    readonly " $data"?: Todo_viewer$data;
    readonly " $fragmentRefs": FragmentRefs<"Todo_viewer">;
};



const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "Todo_viewer",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    }
  ],
  "type": "User"
};
(node as any).hash = '03642422e83eec07dd1fc88cc4ef902a';
export default node;

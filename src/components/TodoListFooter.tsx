import Link from 'found/Link';
import React from 'react';
import { createFragmentContainer, graphql, RelayProp } from 'react-relay';

import RemoveCompletedTodosMutation from '../mutations/RemoveCompletedTodosMutation';
import { TodoListFooter_viewer } from './__generated__/TodoListFooter_viewer.graphql';


interface IProps{
  relay: RelayProp;
  viewer: TodoListFooter_viewer;
}

class TodoListFooter extends React.Component<IProps> {
  onClearCompletedClick = () => {
    const { relay, viewer } = this.props;
    const { todos } = viewer;

    RemoveCompletedTodosMutation.commit(relay.environment, viewer, todos);
  };

  render() {
    const { numTodos, numCompletedTodos } = this.props.viewer;
    if (!this.props.viewer.numTodos) {
      return null;
    }

    return (
      <footer className="footer">
        <span className="todo-count">
          <strong>{numTodos}</strong> {numTodos === 1 ? 'item' : 'items'} left
        </span>

        <ul className="filters">
          <li>
            <Link to="/" activeClassName="selected" exact>
              All
            </Link>
          </li>
          <li>
            <Link to="/active" activeClassName="selected">
              Active
            </Link>
          </li>
          <li>
            <Link to="/completed" activeClassName="selected">
              Completed
            </Link>
          </li>
        </ul>

        {!!numCompletedTodos && (
          <button
            type="button"
            className="clear-completed"
            onClick={this.onClearCompletedClick}
          >
            Clear completed
          </button>
        )}
      </footer>
    );
  }
}

export default createFragmentContainer(TodoListFooter, {
  viewer: graphql`
    fragment TodoListFooter_viewer on User {
      todos(status: "completed", first: 2147483647) {
        edges {
          node {
            id
            complete
          }
        }
      }
      id
      numTodos
      numCompletedTodos
    }
  `,
});

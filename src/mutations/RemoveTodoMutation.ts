import { commitMutation, graphql } from 'react-relay';
import { ConnectionHandler } from 'relay-runtime';
import {
  RemoveTodoMutationVariables,
  RemoveTodoMutationResponse
} from './__generated__/RemoveTodoMutation.graphql'
import { Environment, RecordSourceSelectorProxy } from 'relay-runtime/lib/store/RelayStoreTypes';
import { TodoList_viewer } from '../components/__generated__/TodoList_viewer.graphql';
import { Todo_todo } from '../components/__generated__/Todo_todo.graphql';
import { Todo_viewer } from '../components/__generated__/Todo_viewer.graphql';
interface IMutation {
  response: RemoveTodoMutationResponse;
  variables: RemoveTodoMutationVariables;
}
const mutation = graphql`
  mutation RemoveTodoMutation($input: RemoveTodoInput!) {
    removeTodo(input: $input) {
      viewer {
        numTodos
        numCompletedTodos
      }
      deletedId
    }
  }
`;

function sharedUpdater(
  store: RecordSourceSelectorProxy<RemoveTodoMutationResponse>,
  user: Todo_viewer,
  deletedId: string) {
  const userProxy = store.get(user.id)!;

  ['any', 'active', 'completed'].forEach((status) => {
    const connection = ConnectionHandler.getConnection(
      userProxy,
      'TodoList_todos',
      { status },
    );
    if (connection) {
      ConnectionHandler.deleteNode(connection, deletedId);
    }
  });
}

function commit(environment: Environment, user: Todo_viewer, todo: Todo_todo) {
  return commitMutation<IMutation>(environment, {
    mutation,
    variables: {
      input: { id: todo.id },
    },

    updater(store) {
      const payload = store.getRootField('removeTodo');
      sharedUpdater(store, user, payload.getValue('deletedId'));
    },

    optimisticUpdater(store) {
      sharedUpdater(store, user, todo.id);

      const userProxy = store.get<TodoList_viewer>(user.id)!;
      const numTodos = userProxy.getValue('numTodos');
      if (numTodos != null) {
        userProxy.setValue(numTodos - 1, 'numTodos');
      }
      const numCompletedTodos = userProxy.getValue('numCompletedTodos');
      if (numCompletedTodos != null) {
        if (todo.complete != null) {
          if (todo.complete) {
            userProxy.setValue(numCompletedTodos - 1, 'numCompletedTodos');
          }
        } else if (numTodos != null) {
          // Note this is the old numTodos.
          if (numTodos - 1 < numCompletedTodos) {
            userProxy.setValue(numTodos - 1, 'numCompletedTodos');
          }
        }
      }
    },
  });
}

export default { commit };

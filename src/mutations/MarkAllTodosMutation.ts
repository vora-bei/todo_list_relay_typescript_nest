import { commitMutation, graphql } from 'react-relay';
import { ConnectionHandler } from 'relay-runtime';
import {
  MarkAllTodosMutationVariables,
  MarkAllTodosMutationResponse
} from './__generated__/MarkAllTodosMutation.graphql'
import { Todo_todo } from '../components/__generated__/Todo_todo.graphql';
import { Environment, RecordProxy } from 'relay-runtime/lib/store/RelayStoreTypes';
import { TodoList_viewer } from '../components/__generated__/TodoList_viewer.graphql';
interface IMutation{
  response: MarkAllTodosMutationResponse;
  variables: MarkAllTodosMutationVariables;
}
const mutation = graphql`
  mutation MarkAllTodosMutation($input: MarkAllTodosInput!, $status: String!) {
    markAllTodos(input: $input) {
      viewer {
        todos(status: $status) {
          edges {
            node {
              id
              complete
              text
            }
          }
        }
        id
        numCompletedTodos
      }
      changedTodos {
        id
        complete
      }
    }
  }
`;

function commit(environment: Environment, user: TodoList_viewer, complete: boolean, status: string) {
  return commitMutation<IMutation>(environment, {
    mutation,
    variables: {
      input: { complete },
      status,
    },

    updater(store) {
      const userProxy = store.get<TodoList_viewer>(user.id)!;
      const connection = ConnectionHandler.getConnection(
        userProxy,
        'TodoList_todos',
        { status },
      )!;
      const todoEdges = store
        .getRootField('markAllTodos')
        .getLinkedRecord('viewer')
        .getLinkedRecord('todos', { status })
        .getLinkedRecords('edges');
      connection.setLinkedRecords(todoEdges, 'edges');
    },

    optimisticUpdater(store) {
      const userProxy = store.get<TodoList_viewer>(user.id)!;
      const connection = ConnectionHandler.getConnection(
        userProxy,
        'TodoList_todos',
        { status },
      )!;

      if (
        (complete && status === 'active') ||
        (!complete && status === 'completed')
      ) {
        connection.setLinkedRecords([], 'edges');
      }

      connection.getLinkedRecords<RecordProxy<{node: Todo_todo}>[]>('edges')!.forEach((edge) => {
        edge.getLinkedRecord('node')!.setValue(complete, 'complete');
      });

      userProxy.setValue(
        complete ? userProxy.getValue('numTodos') : 0,
        'numCompletedTodos',
      );
    },
  });
}

export default { commit };

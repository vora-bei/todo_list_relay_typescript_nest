import { commitMutation, graphql } from 'react-relay';
import {
  RenameTodoMutationResponse,
  RenameTodoMutationVariables,
} from './__generated__/RenameTodoMutation.graphql'
import { Environment } from 'relay-runtime/lib/store/RelayStoreTypes';
import { Todo_todo } from '../components/__generated__/Todo_todo.graphql';
interface IMutation {
  response:   RenameTodoMutationResponse;
  variables: RenameTodoMutationVariables;
}
const mutation = graphql`
  mutation RenameTodoMutation($input: RenameTodoInput!) {
    renameTodo(input: $input) {
      todo {
        id
        text
      }
    }
  }
`;

function commit(environment: Environment, todo: Todo_todo, text: string) {
  return commitMutation<IMutation>(environment, {
    mutation,
    variables: {
      input: { id: todo.id, text },
    },

    optimisticResponse: {
      renameTodo: {
        todo: {
          id: todo.id,
          text,
        },
      },
    },
  });
}

export default { commit };

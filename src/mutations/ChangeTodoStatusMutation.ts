import { commitMutation, graphql } from 'react-relay';
import { ConnectionHandler, RecordSourceSelectorProxy, RecordProxy } from 'relay-runtime';
import {
  ChangeTodoStatusMutationVariables,
  ChangeTodoStatusMutationResponse
} from './__generated__/ChangeTodoStatusMutation.graphql'
import { Todo_todo } from '../components/__generated__/Todo_todo.graphql';
import { Todo_viewer } from '../components/__generated__/Todo_viewer.graphql';
import { Environment } from 'relay-runtime/lib/store/RelayStoreTypes';
import { TodoList_viewer } from '../components/__generated__/TodoList_viewer.graphql';
interface IMutation{
  response: ChangeTodoStatusMutationResponse;
  variables: ChangeTodoStatusMutationVariables;
}
const mutation = graphql`
  mutation ChangeTodoStatusMutation($input: ChangeTodoStatusInput!) {
    changeTodoStatus(input: $input) {
      viewer {
        id
        numCompletedTodos
      }
      todo {
        id
        complete
      }
    }
  }
`;

function sharedUpdater(
  store: RecordSourceSelectorProxy<ChangeTodoStatusMutationResponse>,
  user: Todo_viewer,
  todoProxy: RecordProxy<Omit<Omit<Todo_todo," $refType">, "text">>
    ) {
  // In principle this could add to the active connection, but such an
  // interaction is not possible from the front end.
  const userProxy = store.get<TodoList_viewer>(user.id)!;
  const status = todoProxy.getValue('complete') ? 'active' : 'completed';
  const connection = ConnectionHandler.getConnection(
    userProxy,
    'TodoList_todos',
    { status },
  );
  if (connection) {
    ConnectionHandler.deleteNode(connection, todoProxy.getValue('id'));
  }
}

function commit(environment: Environment, user: Todo_viewer, todo: Todo_todo, complete: boolean) {
  return commitMutation<IMutation>(environment, {
    mutation,
    variables: {
      input: { id: todo.id, complete },
    },

    updater(store) {
      const payload = store.getRootField('changeTodoStatus');
      sharedUpdater(store, user, payload.getLinkedRecord('todo'));
    },

    optimisticUpdater(store) {
      const todoProxy = store.get<Todo_todo>(todo.id)!;
      todoProxy.setValue(complete, 'complete');
      sharedUpdater(store, user, todoProxy);

      const userProxy = store.get<TodoList_viewer>(user.id)!;
      const numCompletedTodos = userProxy.getValue('numCompletedTodos');
      if (numCompletedTodos != null) {
        userProxy.setValue(
          numCompletedTodos + (complete ? 1 : -1),
          'numCompletedTodos',
        );
      }
    },
  });
}

export default { commit };

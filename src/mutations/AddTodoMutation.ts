import { commitMutation, graphql } from 'react-relay';
import { ConnectionHandler, RecordSourceSelectorProxy, RecordProxy } from 'relay-runtime';
import {
  AddTodoMutationResponse,
  AddTodoMutationVariables
} from './__generated__/AddTodoMutation.graphql'
import { TodoListFooter_viewer } from '../components/__generated__/TodoListFooter_viewer.graphql'
import { Environment } from 'relay-runtime';
interface IMutation {
  response: AddTodoMutationResponse;
  variables: AddTodoMutationVariables;
}
const mutation = graphql`
  mutation AddTodoMutation($input: AddTodoInput!) {
    addTodo(input: $input) {
      viewer {
        id
        numTodos
      }
      todoEdge {
        cursor
        node {
          id
          complete
          text
        }
      }
    }
  }
`;

function sharedUpdater(
  store: RecordSourceSelectorProxy<AddTodoMutationResponse | TodoListFooter_viewer>,
  user: TodoListFooter_viewer,
  todoEdge: RecordProxy<{}>
  ) {
  const userProxy = store.get<TodoListFooter_viewer>(user.id);

  ['any', 'active'].forEach((status) => {
    const connection = ConnectionHandler.getConnection(
      userProxy!,
      'TodoList_todos',
      { status },
    );
    if (connection) {
      ConnectionHandler.insertEdgeAfter(connection, todoEdge);
    }
  });
}

let nextClientMutationId = 0;

function commit(environment: Environment, user: any, text: string) {
  const clientMutationId = (nextClientMutationId++).toString();

  return commitMutation<IMutation>(environment, {
    mutation,
    variables: {
      input: { text, clientMutationId },
    },

    updater(store) {
      const payload = store.getRootField('addTodo');
      sharedUpdater(store, user, payload!.getLinkedRecord('todoEdge'));
    },

    optimisticUpdater(store) {
      const id = `client:addTodo:Todo:${clientMutationId}`;
      const todo = store.create(id, 'Todo');
      todo.setValue(text, 'text');
      todo.setValue(id, 'id');

      const todoEdge = store.create(
        `client:addTodo:TodoEdge:${clientMutationId}`,
        'TodoEdge',
      );
      todoEdge.setLinkedRecord(todo, 'node');

      sharedUpdater(store, user, todoEdge);

      const userProxy = store.get<TodoListFooter_viewer>(user.id);
      const numTodos = userProxy!.getValue('numTodos');
      if (numTodos != null) {
        userProxy!.setValue(numTodos + 1, 'numTodos');
      }
    },
  });
}

export default { commit };
